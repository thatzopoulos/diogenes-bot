# Diogenes Bot
Lichess Bot written in RUst

## GOALS:
Uses UCI to communicate

## Lichess bot creation
move binary to engines folder of lichess-bot and then run ```python3 lichess-bot.py -u```

## References
https://www.chessprogramming.org/Main_Page

github.com/cutechess/cutechess

## Commands to test bot locally, using stockfish binaries as an example:
 cutechess-cli -engine cmd=./stockfish -engine cmd=./stockfish -each proto=uci tc=40/60 -rounds 10 -debug
